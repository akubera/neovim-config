local plugins = {
  -----------------------
  --- Themes
  ---
  { "lunarvim/darkplus.nvim" },
  { "folke/tokyonight.nvim" },
  { "base16-project/base16-vim" },

  {
    "catppuccin/nvim",
    lazy = false,
    priority = 1000,
    config = function()
      if true then
        vim.cmd("colorscheme catppuccin-mocha")
      else
        vim.cmd("colorscheme catppuccin-latte")
      end
    end,
  },

  -----------------------
  --- UI Components
  ---

  { -- startup screen
    "nvimdev/dashboard-nvim",
    event = "VimEnter",
    opts = require("config/dashboard-opts")
  },

  { -- directory tree in sidebar
    "nvim-tree/nvim-tree.lua",
    lazy = false,
    config = function()
      require("config/file-tree")
    end,
  },

  { -- buffer list at top
    "akinsho/bufferline.nvim",
    version = "v4.x",
    event = { "BufEnter" },
    dependencies = {
      "nvim-tree/nvim-web-devicons",
    },
    config = function()
      require("config/bufferline")
    end,
  },

  { -- file information at bottom
    "nvim-lualine/lualine.nvim",
    lazy = false,
    dependencies = {
      "nvim-tree/nvim-web-devicons",
    },
    config = function()
      require("config/lualine")
    end,
  },

  --   -- UNDO Tree
  --   "mbbill/undotree",


  {
    "christoomey/vim-tmux-navigator",
    cmd = {
      "TmuxNavigateLeft",
      "TmuxNavigateDown",
      "TmuxNavigateUp",
      "TmuxNavigateRight",
      "TmuxNavigatePrevious",
    },
    keys = {
      { "<c-h>",  "<cmd><C-U>TmuxNavigateLeft<cr>" },
      { "<c-j>",  "<cmd><C-U>TmuxNavigateDown<cr>" },
      { "<c-k>",  "<cmd><C-U>TmuxNavigateUp<cr>" },
      { "<c-l>",  "<cmd><C-U>TmuxNavigateRight<cr>" },
      { "<c-\\>", "<cmd><C-U>TmuxNavigatePrevious<cr>" },
    },
  },

  { -- help with key-mappings
    "folke/which-key.nvim",
    tag = "v1.6.0",
    -- lazy = false,
    event = "VeryLazy",
    config = function()
      require("config/whichkey")
    end,
  },

  {
    "folke/neoconf.nvim",
    cmd = "Neoconf",
  },

  -- neovim lua api code completion & other dev-tools
  { "folke/neodev.nvim" },

  {
    "folke/zen-mode.nvim",
    config = function()
      require("config/zen-mode")
    end,
  },


  ----------------------
  --- Git Integration
  ---

  {
    "tpope/vim-fugitive",
    lazy = false,
    --keys = { "<leader>g" },
    --config = function() require("config/fugitive") end
  },


  {
    "lewis6991/gitsigns.nvim",
    lazy = true,
    keys = { "<leader>g" },
    config = function()
      require("config/gitsigns")
    end,
  },

  {
    "monaqa/dial.nvim",
    keys = { "<C-a>", { "<C-x>", mode = "n" } },
  },

  {
    "nvim-telescope/telescope.nvim",
    lazy = false,
    -- tag = '0.1.5',
    dependencies = {
      -- '3rd/image.nvim',
      "nvim-lua/plenary.nvim",
      "nvim-lua/popup.nvim",
      "nvim-telescope/telescope-media-files.nvim",
    },
    opts = {
      file_ignore_patterns = {
        "node%_modules/.*",
        "__pycache__/.*",
        "target/.*",
      },
    },
    config = function()
      require("config/telescope")
    end,
  },

  --   -- { "nvim-neo-tree/neo-tree.nvim",
  --   --       branch = "v3.x",
  --   --       dependencies = {
  --   --         "nvim-lua/plenary.nvim",
  --   --         "nvim-tree/nvim-web-devicons",
  --   --         "MunifTanjim/nui.nvim",
  --   --       },
  --   --       lazy = false,
  --   --       keys = {
  --   --         -- { "<leader>to", "<cmd>Neotree toggle<cr>", desc = "NeoTree" },
  --   --         { "<C-n>", desc = "Neotree" }
  --   --       },
  --   --       config = function() require("config/file-tree") end },
  --
  --   {
  --     "echasnovski/mini.comment",
  --     version = "*",
  --     config = true
  --   },
  --   {
  --     "echasnovski/mini.indentscope",
  --     version = "*",
  --     config = true
  --   },
  --
  --   {
  --     "tpope/vim-fugitive",
  --     lazy = false,
  --   },
  --
  --   {
  --     "akinsho/toggleterm.nvim",
  --     lazy = false,
  --     version = "*",
  --     config = function() require("config/toggleterm") end,
  --   },
  --
  --
  --   {
  --     "andythigpen/nvim-coverage",
  --     version = "*",
  --     config = function() require("config/coverage") end
  --   },
  --
  --   --- TreeSitter
  -- --  {
  -- --    "nvim-treesitter/nvim-treesitter",
  -- --    dependencies = {
  -- --      "nvim-treesitter/nvim-treesitter-textobjects",
  -- --      "neovim/tree-sitter-vim",
  -- --      "nvimdev/lspsaga.nvim",
  -- --    },
  -- --    config = function() require("config/treesitter") end
  -- --  },

  {
    "neovim/nvim-lspconfig",
    dependencies = {
      "nvimdev/lspsaga.nvim",
      "nvim-treesitter/nvim-treesitter",
      "nvim-tree/nvim-web-devicons",
      "nvimtools/none-ls.nvim",
      "williamboman/mason.nvim",
    },
    config = function()
      require("config/lsp")
    end,
  },

  {
    "williamboman/mason.nvim",
    dependencies = {
      "williamboman/mason-lspconfig.nvim",
      -- "jayp0521/mason-nvim-dap.nvim",
    },
  },

  --
  --
  --   -- clipboard management
  --   "gbprod/yanky.nvim",
  --   -- 'gennaro-tedesco/nvim-peekup',
  --   -- "tversteeg/registers.nvim",
  --
  --
  --   --- commands as a popup
  --   {
  --     "folke/noice.nvim",
  --     dependencies = {
  --       "rcarriga/nvim-notify",
  --     }
  --   },
  --
  --   --- project management
  --   -- { "ahmedkhalf/project.nvim",
  --   --   lazy = false,
  --   --   config = function() require("project_nvim").setup({ }) end }
  --   -- {
  --   --    "Shatur/neovim-session-manager",
  --   --    lazy = false,
  --   -- },
  --
  --   -- {
  --   --   "coffebar/neovim-project",
  --   --   opts = {
  --   --     projects = {
  --   --       "~/development/*/*",
  --   --       "~/.config/nvim",
  --   --     },
  --   --     init = function()
  --   --       -- enable saving the state of plugins in the session
  --   --       vim.opt.sessionoptions:append("globals") -- save global variables that start with an uppercase letter and contain at least one lowercase letter.
  --   --     end,
  --   --     dependencies = {
  --   --       { "nvim-lua/plenary.nvim" },
  --   --       { "nvim-telescope/telescope.nvim", tag = "0.1.4" },
  --   --       { "Shatur/neovim-session-manager" },
  --   --     },
  --   --     lazy = false,
  --   --     priority = 100,
  --   --   }
  --   -- },
  --
  --   --- Terminal
  --   {
  --     "voldikss/vim-floaterm",
  --     lazy = true,
  --   },
  --
  --
  --   --- CMP
  --   {
  --     "hrsh7th/nvim-cmp",
  --     dependencies = {
  --       "hrsh7th/cmp-buffer",
  --       "hrsh7th/cmp-path",
  --       "hrsh7th/cmp-cmdline",
  --       "hrsh7th/cmp-nvim-lsp",
  --     },
  --     config = function() require("config/completion") end
  --   },
  --
  --   --- ORG-Mode
  --   {
  --     "nvim-orgmode/orgmode",
  --     event = 'VeryLazy',
  --     dependencies = {
  --       'nvim-treesitter/nvim-treesitter'
  --     },
  --     config = function() require("config/org-mode") end
  --   },
  --
}

local lazy_opts = {}

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local lazy = require("lazy")
lazy.setup(plugins, lazy_opts)
