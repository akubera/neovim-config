
local opts = { noremap = true, silent = true }
local term_opts = { silent = true }

local keymap = vim.api.nvim_set_keymap
local map = vim.keymap.set

keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

--- These are now handled by vim-tmux-navigator plugin
--- Better window navigation
--keymap("n", "<C-h>", "<C-w>h", opts)
--keymap("n", "<C-j>", "<C-w>j", opts)
--keymap("n", "<C-l>", "<C-w>l", opts)
--keymap("n", "<C-k>", "<C-w>k", opts)

-- Better terminal navigation
keymap("t", "<C-h>", "<C-\\><C-N><C-w>h", term_opts)
keymap("t", "<C-j>", "<C-\\><C-N><C-w>j", term_opts)
keymap("t", "<C-k>", "<C-\\><C-N><C-w>k", term_opts)
keymap("t", "<C-l>", "<C-\\><C-N><C-w>l", term_opts)

-- Resize with arrows
keymap("n", "<C-Up>", ":resize +2<CR>", opts)
keymap("n", "<C-Down>", ":resize -2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

-- Navigate buffers
keymap("n", "<S-l>", ":bnext<CR>", opts)
keymap("n", "<S-h>", ":bprevious<CR>", opts)

-- jk to escape insert
-- keymap("i", "jk", "<Esc>", opts)

-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- Move text up and down
keymap("v", "<A-j>", ":m .+1<CR>==gv", opts)
keymap("v", "<A-k>", ":m .-2<CR>==gv", opts)
keymap("v", "p", '"_dP', opts)

keymap("x", "J", ":move '>+1<CR>gv-gv", opts)
keymap("x", "K", ":move '<-2<CR>gv-gv", opts)
keymap("x", "<A-j>", ":move '>+1<CR>gv-gv", opts)
keymap("x", "<A-k>", ":move '<-2<CR>gv-gv", opts)

-- buffer navigation
keymap("n", "<A-j>", "<cmd>bp<CR>", opts)
keymap("n", "<A-k>", "<cmd>bn<CR>", opts)

-- keymap("n", "<C-\\>", "<CMD>CommentLine<CR>", opts)
-- keymap("n", "<C-/>", "<CMD>CommentLine<CR>", opts)
-- keymap("n", "<C-/>", function() "<CMD>CommentLine<CR>", opts)

local mini_comment_installed, mini_comment = pcall(require, "mini/comment")
if mini_comment_installed then
  map("n", "<C-/>", function() return mini_comment.operator('line') end, {expr = true, desc = "Comment"})
  --map("n", "<C-/>", function() print("COMMENT"); return mini_comment.operator() .. '_' end, {expr = true, desc = "Comment Line"})
  map("x", "<C-/>", [[:<c-u>lua MiniComment.operator('visual')<cr>]], {desc = "Comment selection"})
  map("o", "<C-/>", [[:<c-u>lua MiniComment.textobject()<cr>]], {desc = "Comment textobject"})
end

--
-- local function map(mode, lhs, rhs, desc, opts)
--     opts = opts or { slient = true }
--     opts.desc = desc
--     vim.keymap.set(mode, lhs, rhs, opts)
-- end

map('n', '<leader>p', '"+p', { silent = true, desc = 'Paste clipboard after cursor' })
map('n', '<leader>P', '"+P', { silent = true, desc = 'Paste clipboard before cursor' })

map('', '<leader>y', '"+y', { desc = 'Yank to clipboard' }) -- E.g: <leader>yy will yank current line to os clipboard
map('', '<leader>Y', '"+y$', { desc = 'Yank until EOL to clipboard' })

map('', '<leader>h', '<CMD>noh<CR>', { desc = 'disable highlighting' })

map('n', '<A-F>', vim.lsp.buf.format, opts)


-- local toggle_term = function()
--   vim.cmd [[ToggleTerm]]
-- end

-- map('n', '<C-`>', toggle_term, opts)
-- map('t', '<C-`>', toggle_term, opts)


local whichkey_installed, wk = pcall(require, "which-key")

local M = {}

function M.setup_bufferline(bufferline)
   -- print(bufferline.cyle)
   -- for k,v in pairs(bufferline) do
   --   print(k, v)
   -- end
   -- map('n', "<A-l>", [[ bufferline.cycle.next ]])

  map('n', "<A-h>", [[ <cmd> BufferLineCyclePrev <CR> ]]) -- Alt+j to move to left
  map('n', "<A-l>", [[ <cmd> BufferLineCycleNext <CR> ]]) -- Alt+k to move to right

  if whichkey_installed then
      wk.register({
        b = {
          name = "Buffers",
          c = {
            name = "Close",
            c = { function() bufferline.close() end , "Close" },
            R = { function() bufferline.close_in_direction('right') end , "Close All Right" },
            L = { function() bufferline.close_in_direction('left') end , "Close All Left" },
            o = { bufferline.close_others , "Close Others" },
          },
          n = { function() bufferline.move(1) end, "Next" },
          p = { function() bufferline.move(-1) end, "Previous" },
        },
      }, { prefix = "<leader>" })
  end
  --
  -- map('n', "<leader> bcr ", function() bufferline.close_in_direction('right') end, {desc = "Close in direction right"}) -- Alt+k to move to right
  -- map('n', "<leader> bcr ", [[ <cmd> BufferLineCloseRight <CR> ]]) -- Alt+k to move to right
  -- map('n', '<A-j>', bufferline.BufferLineCyclePrev)
  -- map('n', '<A-j>', '<cmd>BufferLineCyclePrev<CR>')
  -- map('n', '<A-k>', '<cmd>BufferLineCycleNext<CR>')
   map('n', '<A-J>', [[ <cmd> BufferLineMovePrev <CR> ]])  -- Alt+Shift+j grab to with you to left
   map('n', '<A-K>', [[ <cmd> BufferLineMoveNext <CR> ]])  -- Alt+Shift+k grab to with you to right
end


function M.setup_toggleterm(toggleterm)
  map("n", '<C-`>', toggleterm.toggle)
  map("t", '<C-`>', toggleterm.toggle)
end


function M.setup_telescope(builtin, actions)
  map('n', "<leader>kt", builtin.colorscheme, { silent = true, desc = "Change color theme" })

  -- map('n', "<leader>ff", builtin.find_files, opts)
  map('n', "<leader>ff", function() builtin.find_files({ hidden = true }) end, opts)
  map('n', "<leader>fo", builtin.oldfiles, { silent = true, desc = "Find old files" })
  map('n', "<leader>fg", builtin.live_grep, opts)
  map('n', "<leader>fb", builtin.buffers, opts)
  map('n', "<leader>fh", builtin.help_tags, opts)

  if whichkey_installed then
      wk.register({
        b = {
          --name = "+BB?",
          f = { builtin.buffers, "Search buffers" },
        }
    }, { prefix = "<leader>" })
  end

  return

      wk.register({
        f = {
          name = "Find",
          f = { builtin.find_files, "Find File" },
          g = { builtin.live_grep, "Live grep" },
          b = { builtin.buffers, "Search buffers" },
          h = { builtin.help_tags, "Search help pages" },
        },
      }, { prefix = "<leader>" })
end

function M.setup_coverage(coverage)
  if whichkey_installed then
    wk.register({
      C = {
        name = "Coverage",
        C = { coverage.load, "Coverage load" },
        T = { coverage.toggle, "Coverage toggle" },
        S = { coverage.summary, "View coverage summary" },
      }
    }, { prefix = "<leader>" })
  end
end

function M.setup_gitsigns(gitsigns)
  map("n", "<leader>gp", gitsigns.preview_hunk)
  map("n", "<leader>gt", gitsigns.toggle_current_line_blame)
end

function M.setup_nvimtree(api)
  map("n", "<C-n>", api.tree.toggle, { silent = true, noremap = true, desc = "Toggle File Tree" })
  --vim.api.nvim_set_keymap("n", "<C-n>", "<CMD>NvimTreeToggle<cr>", {silent = true, noremap = true})

  --keymap('n', "<C-n>", , opts)
  --keymap('n', "<C-n>", "<CMD>Neotree<CR>", opts)
end

function M.setup_lsp(lspconfig)
  vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, { desc = "Open Float" })
  -- vim.keymap.set('n', '<space>f', function() vim.lsp.buf.format { async = true } end, opts)
end

function M.setup_neotree(neotree)
  keymap('n', "<C-n>", "<CMD>Neotree<CR>", opts)

  -- if whichkey_installed then
  --   wk.register({
  --     n = "Open Neotree"
  --   })
  -- end
end

function M.setup_zenmode(zen_mode)
  vim.keymap.set("n", "<leader>zz", zen_mode.toggle, { noremap = true, silent = true })
end

if not whichkey_installed then
  return M
end


-- wk.register({
--   e = { "<CMD>Lex 30<cr>", "Open Tree" },
-- }, { prefix = "<leader>" })

-- keymap("n", "<leader>e", ":Lex 30<cr>", opts)

----------------------
-- WHICH KEY CONFIGURATION

wk.register({
  ["gd"] = { vim.lsp.buf.definition, "go to definition" },
  ["gt"] = { "next tab" },
  ["gt"] = { "previous tab" },
})

return M
