
local coverage = require("coverage")

coverage.setup({
  commands = true,
})

require("keymaps").setup_coverage(coverage)
