gitsigns = require("gitsigns")
gitsigns.setup()

require("keymaps").setup_gitsigns(gitsigns)
