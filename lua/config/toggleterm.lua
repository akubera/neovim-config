local toggleterm = require("toggleterm")

toggleterm.setup {}

require("keymaps").setup_toggleterm(toggleterm)
