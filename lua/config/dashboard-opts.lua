--- Configure startup screen (dashboard)
return function()
  local fortune = ""
  local fortune_called, fortune_cmd = pcall(io.popen, "fortune -s | cowsay")
  if fortune_called and fortune_cmd ~= nil then
    fortune = fortune_cmd:read("*a")
    fortune_cmd:close()
  end

  local header = fortune .. "\n\n"

  local opts = {
    theme = "doom",
    hide = {
      statusline = false
    },
    config = {
      header = vim.split(header, "\n")
    }
  }

  return opts
end
