-- help
local ok, telescope = pcall(require, "telescope")
if not ok then
  return
end

local actions = require("telescope.actions")
local builtin = require("telescope.builtin")

telescope.setup({
  defaults = {
    -- layout_strategy = "bottom_pane",
    layout_strategy = "horizontal",
    extensions_list = {
      "themes", "terms", "fzf"
    },
    mappings = {
      i = {
        ["<c-j>"] = actions.move_selection_next,
        ["<c-k>"] = actions.move_selection_previous,
        -- ["<c-n>"] = "<Esc>",
        ["<Esc>"] = actions.close,
      }
    },

    --[[
    preview = {
      mime_hook = function(filepath, bufnr, opts)
        local api = require "image"
        local preview_id = "telescope_image_id"
        api.clear(preview_id)
        local is_image = function(filepath)
          local image_extensions = { "png", "jpg", "jpeg", "gif" } -- Supported image formats
          local split_path = vim.split(filepath:lower(), ".", { plain = true })
          local extension = split_path[#split_path]
          return vim.tbl_contains(image_extensions, extension)
        end

        if is_image(filepath) then
          local image = api.from_file(filepath, {
            id = preview_id,
            buffer = bufnr,
            x = 110,
            y = 20,
            width = 40,
            height = 40,
          })
          if image ~= nil then
            print("image = " .. vim.inspect(image))
            image:render()
          end
        else
          require("telescope.previewers.utils").set_preview_message(
                bufnr, opts.winid, "Something else"
          )
        end
      end,
    },
    ]] --

  },

  extensions = {
    ["ui-select"] = {
      require("telescope.themes").get_dropdown({}),
    },
  },

  pickers = {
    buffers = {
      mappings = {
        i = {
          ["<c-d>"] = actions.delete_buffer,
        },
        n = {
          ["<c-d>"] = actions.delete_buffer,
        },
      }
    },
  },
})

-- telescope.load_extension "ui-select"

local media_files_installed, _ = pcall(telescope.load_extension, "media-files")
if media_files_installed then
  telescope.extensions.media_files.media_files()
end

pcall(telescope.load_extension, "image")

local image_installed, image = pcall(require, "image")
if image_installed then
  image.setup({})
end

-- pcall(telescope.load_extension, 'projects')

require('keymaps').setup_telescope(builtin, actions)
