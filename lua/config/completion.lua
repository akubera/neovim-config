local installed, cmp = pcall(require, 'cmp')
if not installed then
  return
end


cmp.setup({
  sources = {
    { name = 'orgmode' }
  }
})
