local bufferline = require("bufferline")

bufferline.setup({
  options = {
    themable = true,
    -- numbers = "buffer_id",
    numbers = "ordinal",
    indicator = {
      style = "underline",
    },
    offsets = {
      {
        filetype = "NvimTree",
        text = "File Explorer",
        highlight = "Directory",
        separator = true, -- use a "true" to enable the default, or set your own character
      },
    },
  },
})

require("keymaps").setup_bufferline(bufferline)
