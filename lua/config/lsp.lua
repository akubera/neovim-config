local lspconfig = require('lspconfig')

-- lspconfig.pyright.setup {}
-- lspconfig.rust_analyzer.setup {}
-- lspconfig.lua_ls.setup {
--   settings = {
--     Lua = {
--       diagnostics = {
--         globals = {
--           'vim',
--           'require',
--         },
--       }
--     }
--   },
--   --  cmd = {"/opt/local/bin/lua-language-server"};
-- }


local nullls_is_installed, null_ls = pcall(require, "null-ls")

if nullls_is_installed then
  null_ls.setup({
    sources = {
      null_ls.builtins.formatting.stylua,
      null_ls.builtins.formatting.black,
      -- null_ls.builtins.formatting.ruff,
    },
  })
end


local ok, mason_lsp, mason_dap

local mason_installed, mason = pcall(require, "mason")
if mason_installed then
  mason.setup()
end


ok, mason_lsp = pcall(require, "mason-lspconfig")
if ok then
  mason_lsp.setup()
end

ok, mason_dap = pcall(require, "mason-nvim-dap")
if ok then
  mason_dap.setup()
end


local lspsaga_installed, lspsaga = pcall(require, 'lspsaga')

if lspsaga_installed then
  lspsaga.setup({
    symbol_in_winbar = {
      enable = true
    }
  })
end


-- try loading local lsp config
pcall(require, "lsp-local")

require("keymaps").setup_lsp(lspconfig)
