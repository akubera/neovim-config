local lualine = require('lualine')

for k, v in ipairs(lualine.get_config()) do
  print(k, v)
end

lualine.setup({
  options = {
    theme = 'onedark'
  },
  sections = {
    lualine_a = {'mode'},
    --lualine_b = {'branch', 'diagnostics'},
    lualine_b = {},
    lualine_c = {'diagnostics'},
    --lualine_x = {'encoding', 'fileformat', 'filetype'},
    --lualine_y = {'progress'},
    lualine_x = {},
    lualine_y = {},
    lualine_z = {'location'}
  },
  -- inactive_sections = {
  --   lualine_a = {},
  --   lualine_b = {},
  --   lualine_c = { 'filename' },
  --   lualine_x = {},
  --   lualine_y = {},
  --   lualine_z = {}
  -- },
  extensions = {
    "nvim-tree",
    "lazy",
    "fugitive",
  }
})

