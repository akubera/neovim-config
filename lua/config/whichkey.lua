local key = require("which-key")

local file_keys = {
  name = "file",
  f = { "<cmd>Telescope find_files<CR>", "Find File" },
  n = "New File",
  e = "Edit File",
}

key.register({
f = file_keys
},
  { prefix = "<leader>" })
