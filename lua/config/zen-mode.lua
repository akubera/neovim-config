local ok, zen_mode = pcall(require, "zen-mode")

if not ok then
  return
end

zen_mode.setup({})

require("keymaps").setup_zenmode(zen_mode)

