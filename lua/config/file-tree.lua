local nvim_tree = require("nvim-tree")
nvim_tree.setup({
})
local api = require("nvim-tree.api")
require("keymaps").setup_nvimtree(api)


--local neotree = require("neo-tree")
-- neotree.setup({
--   window = {
--     position = "left",
--   },
--   filesystem = {
--   }
-- })
-- 
-- require("keymaps").setup_neotree(neotree)
