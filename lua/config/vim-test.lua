
local installed, vim_test = pcall(require, "vim-tests")
if installed then
  return
end

vim.cmd [[
  let test#strategy = "vimux"
]]

vim.keymap.set('n', '<leader>t', ':TestNearest<CR>')
vim.keymap.set('n', '<leader>T', ':TestFile<CR>')
