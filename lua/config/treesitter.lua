local installed, treesitter = pcall(require, "nvim-treesitter")
if not installed then
  return
end

treesitter.setup({
})

local configs = require("nvim-treesitter.configs")

configs.setup({
  ensure_installed = {
    "c",
    "regex",
    "bash",
    "json",
    "lua",
    "vimdoc",
    "query",
    "rust",
    "python",
    "markdown",
    "markdown_inline",
    "vim",
    "yang",
  },

  ignore_install = { "" },

  highlight = {
    enable = true,
    disable = {},
    additional_vim_regex_highlighting = true,
  },

  autopairs = {
    enable = true,
  },

  indent = {
    enable = true,
    disable = {
      "python"
    },
  },

  build = function()
        require("nvim-treesitter.install").update({ with_sync = true })()
    end,
})

vim.treesitter.language.add('python')
vim.treesitter.language.add('vim')

