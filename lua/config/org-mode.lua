local orgmode = require('orgmode')

orgmode.setup({
  org_agenda_files = '~/orgfiles/**/*',
  org_default_notes_file = '~/orgfiles/refile.org',
})

-- Load treesitter grammar for org
orgmode.setup_ts_grammar()

-- Setup treesitter
require('nvim-treesitter.configs').setup({
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = { 'org' },
  },
  ensure_installed = { 'org' },
})

local cmp_installed, cmp = pcall(require, 'cmp')
if cmp_installed then
  cmp.setup({
    sources = {
      { name = 'orgmode' }
    }
  })
end
